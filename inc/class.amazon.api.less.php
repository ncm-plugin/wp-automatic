<?php

/**
 * Class to scrape amazon products
 * @author Muhammed Atef
 * @link http://www.deandev.com
 * @version 1.0
 */

/*
 * From Jan 2019 amazon has changed it's api usage policy. API feature is mapped with sale you made in last month.
 * https://docs.aws.amazon.com/es_es/AWSECommerceService/latest/DG/TroubleshootingApplications.html
 *
 */
class wp_automatic_amazon_api_less {
	/**
	 * cURL handle
	 */
	private $ch = "";
	
	/**
	 * Your Amazon Associate Tag
	 * Now required, effective from 25th Oct.
	 * 2011
	 *
	 * @access private
	 * @var string
	 */
	private $associate_tag = "YOUR AMAZON ASSOCIATE TAG";
	private $region = "";
	function __construct(&$ch) {
		$this->ch = $ch;
		$this->associate_tag = $ass;
		$this->region = $region;
	}
	
	/**
	 * Return details of a product searched by ASIN
	 *
	 * @param int $asin_code
	 *        	ASIN code of the product to search
	 * @return mixed simpleXML object
	 */
	public function getItemByAsin($asin_code) {
	}
	
	/**
	 * Return details of a product searched by keyword
	 *
	 * @param string $keyword
	 *        	keyword to search
	 * @param string $product_type
	 *        	type of the product
	 * @return mixed simpleXML object
	 */
	public function getItemByKeyword($keyword, $ItemPage, $product_type, $additionalParam = array(), $min = '', $max = '') {
		
		// encoded keyword
		$keyword_encoded = urlencode ( trim ( $keyword ) );
		
		$search_url = "https://www.amazon.co.uk/s?k=$keyword_encoded&ref=nb_sb_noss";
		
		// https://www.amazon.co.uk/s?k=iphone&ref=nb_sb_noss
		if ($ItemPage != 1) {
			
			// https://www.amazon.co.uk/s?k=iphone&page=2&qid=1556560188&ref=sr_pg_2
		}
		
		// curl get
		$x = 'error';
		$url = $search_url;
		curl_setopt ( $this->ch, CURLOPT_HTTPGET, 1 );
		curl_setopt ( $this->ch, CURLOPT_URL, trim ( $url ) );
		$exec = curl_exec ( $this->ch );
		$x = curl_error ( $this->ch );
		
		// validate returned content
		if (trim ( $exec ) == '' || trim ( $x ) != '') {
			throw new Exception ( 'No valid reply returned from Amazon with a possible cURL err ' . $x );
		}
		
		
		
	}
	
	/**
	 * Return list of ASINs of items by scraping the page
	 *
	 * @param string $pageUrl
	 * @param string $ch
	 *        	curl handler
	 */
	public function getASINs($moreUrl, &$ch) {
		
		// curl get
		$x = 'error';
		$url = $moreUrl;
		curl_setopt ( $ch, CURLOPT_HTTPGET, 1 );
		curl_setopt ( $ch, CURLOPT_URL, trim ( $url ) );
		
		$exec = curl_exec ( $ch );
		$x = curl_error ( $ch );
		
		// validate reply
		if (trim ( $exec ) == '') {
			throw new Exception ( 'Empty reply from Amazon with possible curl error ' . $x );
		}
		
		// validate products found
		if (! stristr ( $exec, 'data-asin' )) {
			// throw new Exception('No items found') ;
			echo '<br>No items found';
			return array ();
		}
		
		// extract products
		preg_match_all ( '{data-asin="(.*?)"}', $exec, $productMatchs );
		$asins = $productMatchs [1];
		
		if (stristr ( $exec, 'proceedWarning' )) {
			echo '<br>Reached end page of items';
			return array ();
		}
		
		return ($asins);
	}
}

?>